
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0pky">Title:</th>
    <th class="tg-0pky">Easy Hotel Booking</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">Objective:</td>
    <td class="tg-0pky">I want to be able to book a room online, choose the type of room, and specify the duration of my stay for easy booking.</td>
  </tr>
  <tr>
    <td class="tg-0pky">Scenario:</td>
    <td class="tg-0pky">As a user (adsqw), I want the ability to book a room online to save time and convenience.</td>
  </tr>
  <tr>
    <td class="tg-0pky">Main Steps:    </td>
    <td class="tg-0pky">1. I access the room booking application or website.<br>2. I log in to my account <br>3. I select the "Book Room Online" or a similar option.<br>4. I select the date and time of my stay.<br>5. I review the information for accuracy and confirm the booking.</td>
  </tr>
  <tr>
    <td class="tg-0pky">Expected Result:</td>
    <td class="tg-0pky">After confirming the booking, I receive a confirmation with detailed information including the room type, date, and time of stay.</td>
  </tr>
  <tr>
    <td class="tg-0pky">Acceptance Criteria:</td>
    <td class="tg-0pky">I receive a confirmation with accurate information.<br>I can review the booking datails after complection</td>
  </tr>
  <tr>
    <td class="tg-0pky">Note:</td>
    <td class="tg-0pky">In case there are no available rooms or an error occurs during the booking process, the application or website should provide a clear error message and guide me on how to resolve it.</td>
  </tr>
</tbody>
</table>